package com.example.workmanager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private EditText water, exercise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button oneTimeWater = findViewById(R.id.one_time_water);
        Button oneTimeExercise = findViewById(R.id.one_time_exercise);
        Button periodicWater = findViewById(R.id.periodic_water);
        Button periodicExercise = findViewById(R.id.periodic_exercise);
        final Button cancelAll = findViewById(R.id.cancel_all);

        oneTimeWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setOneTimeWater();
            }
        });

        oneTimeExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setOneTimeExercise();
            }
        });

        periodicExercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPeriodicExercise();
            }
        });

        periodicWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPeriodicWater();
            }
        });

        cancelAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelAll();
            }
        });

        water = findViewById(R.id.water_alarm_time);
        exercise = findViewById(R.id.exercise_alarm_time);

    }

    private void cancelAll() {
        WorkManager.getInstance(this).cancelAllWork();
    }

    private void setOneTimeWater() {
        if (water.getText() != null) {
            int duration = Integer.parseInt(water.getText().toString());
            OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(WaterWork.class)
                    .setInitialDelay(duration, TimeUnit.MINUTES)
                    .build();
            setWork(workRequest, true);
        }
    }

    private void setOneTimeExercise() {
        if (exercise.getText() != null) {
            int duration = Integer.parseInt(exercise.getText().toString());
            OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(ExerciseWork.class)
                    .setInitialDelay(duration, TimeUnit.MINUTES)
                    .build();
            setWork(workRequest, false);
        }
    }

    private void setPeriodicWater() {
        if (water.getText() != null) {
            int duration = Integer.parseInt(water.getText().toString().length() > 0 ? water.getText().toString() : "0");
            if (duration * 60 * 1000 < PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS) {
                Toast.makeText(MainActivity.this, "Period too short", Toast.LENGTH_LONG).show();
            } else {
                PeriodicWorkRequest workRequest = new PeriodicWorkRequest.Builder(WaterWork.class, duration,
                        TimeUnit.MINUTES, PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS , TimeUnit.MILLISECONDS).build();
                setWork(workRequest, true);
            }
        }
    }

    private void setPeriodicExercise() {
        if (exercise.getText() != null) {
            int duration = Integer.parseInt(exercise.getText().toString().length() > 0 ? exercise.getText().toString() : "0");
            if (duration * 60 * 1000 < PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS) {
                Toast.makeText(MainActivity.this, "Period too short", Toast.LENGTH_LONG).show();
            } else {
                PeriodicWorkRequest workRequest = new PeriodicWorkRequest.Builder(ExerciseWork.class, duration,
                        TimeUnit.MINUTES, PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS, TimeUnit.MILLISECONDS).build();
                setWork(workRequest, false);
            }
        }
    }

    private void setWork(PeriodicWorkRequest work, boolean water) {
        if (water) {
            String PERIODIC_WATER = "periodic_water";
            WorkManager.getInstance(this).enqueueUniquePeriodicWork(PERIODIC_WATER, ExistingPeriodicWorkPolicy.REPLACE, work);
            Log.i("SOME_TAG", "Success water");
        } else {
            String PERIODIC_EXERCISE = "periodic_exercise";
            WorkManager.getInstance(this).enqueueUniquePeriodicWork(PERIODIC_EXERCISE, ExistingPeriodicWorkPolicy.REPLACE, work);
            Log.i("SOME_TAG", "Success exercise");
        }
    }

    private void setWork(OneTimeWorkRequest work, boolean water) {
        if (water) {
            String ONE_TIME_WATER = "one_time_water";
            WorkManager.getInstance(this).enqueueUniqueWork(ONE_TIME_WATER, ExistingWorkPolicy.REPLACE, work);
            Log.i("SOME_TAG", "Success water");
        } else {
            String ONE_TIME_EXERCISE = "one_time_exercise";
            WorkManager.getInstance(this).enqueueUniqueWork(ONE_TIME_EXERCISE, ExistingWorkPolicy.REPLACE, work);
            Log.i("SOME_TAG", "Success exercise");
        }
    }

}